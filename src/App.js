import React, { useEffect, useState } from "react";
import axios from "axios";
import logo from "./logo.svg";

import { Route, Switch } from "react-router-dom";
import "./App.css";

function App() {
  const [routes, setRoutes] = useState([]);
  useEffect(() => {
    axios.get("http://localhost:3333/selling").then(({ data }) => {
      setRoutes(data);
    });
  }, []);
  return (
    <div className="App">
      <Switch>
        {routes.map(r => (
          <Route path={`/${r.path}`}>
            <Common src={r.src} />
          </Route>
        ))}
      </Switch>
    </div>
  );
}

function Common({ src = "https://i.picsum.photos/id/1080/200/300.jpg" }) {
  return <img width="200" height="200" src={src} />;
}

export default App;
