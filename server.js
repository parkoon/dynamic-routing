const express = require("express");
const cors = require("cors");

const app = express();

app.use(cors());
const sellingPages = [
  {
    path: "1",
    src: "https://i.picsum.photos/id/1001/200/300.jpg"
  },
  {
    path: "2",
    src: "https://i.picsum.photos/id/1002/200/300.jpg"
  },
  {
    path: "3",
    src: "https://i.picsum.photos/id/1003/200/300.jpg"
  },
  {
    path: "4",
    src: "https://i.picsum.photos/id/1004/200/300.jpg"
  }
];

app.get("/selling", (req, res) => {
  res.json(sellingPages);
});

app.listen(3333);
